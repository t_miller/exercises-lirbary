# Exercise Library

An application to show various exercises to the user in a clear and concise mannor. 
This application allows the user to filter the type of exercise and add / remove them to their own exercises diary.

This is a single page application built with React and styled components. 
Using cross-fetch to fetch the data and html-react-parser to parse the stringified html.


## Development

If I was to further develop this application further my next tasks would be to introduce a dotenv file and more the url into it.

I'd add either lazyloading or create a image component which would have a placeholder svg and render the image onload.

Next I would look to improve the user's experience, for example I would add an exercise diary counter next to the button.

I would also look to include redux to allow the application to run offline.


## Installation and Setup


You will need node.js and npm installed globally on your machine.

### steps

	- Clone this repository
	
	- Run "npm install" to install the packages
	
	- Run "npm start"
	
	- Visit localhost:3000
	


