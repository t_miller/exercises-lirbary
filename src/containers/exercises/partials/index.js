import styled from "styled-components";

export const StyledContainer = styled.div`
  margin-top: 2rem;
`;

export const StyledListContainer = styled.div`
  width: 100%;

  @media (min-width: ${props => props.theme.breakPoints.wide}) {
    width: 80%;
    align-self: center;
  }
`;

export const StyledInner = styled.div`
  display: flex;
  justify-content: space-between;
  margin: 1rem 0;
`;

export const StyledTag = styled.div`
  width: fit-content;

  @media (max-width: ${props => props.theme.breakPoints.wide}) {
    margin: 0.4rem 0.5rem;
  }

  & div {
    margin: 0;
    cursor: pointer;
    ${props =>
      props.isSelected ? `background: ${props.theme.colors.gray};` : ""}
  }
  & span {
    ${props => (props.isSelected ? `color: ${props.theme.colors.white};` : "")}
  }
`;
