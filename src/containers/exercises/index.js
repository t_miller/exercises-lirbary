import React, { useState } from "react";
import {
  Container,
  ExercisesList,
  ExercisesFilter,
  Switch,
  Hero,
  Tag
} from "../../components";

import { StyledContainer, StyledInner, StyledTag } from "./partials";

const Exercises = () => {
  const [selectedArr, SetSelectedArr] = useState([]);
  const [isMale, SetIsMale] = useState(true);
  const [showDiary, SetShowDiary] = useState(false);

  const selectedHandler = value => {
    const newArr = selectedArr.includes(value)
      ? selectedArr.filter(e => e !== value)
      : [...selectedArr, value];
    SetSelectedArr(newArr);
  };

  const isMaleHandler = () => {
    SetIsMale(!isMale);
  };

  const showDiaryHandler = () => {
    SetShowDiary(!showDiary);
  };

  return (
    <Container>
      <Hero title="Exercise library" />
      <StyledContainer>
        <StyledInner>
          <StyledTag onClick={showDiaryHandler} isSelected={showDiary}>
            <Tag label="My exersice diary" />
          </StyledTag>
          <Switch isMale={isMale} switchHandler={isMaleHandler} />
        </StyledInner>
        {!showDiary && (
          <ExercisesFilter
            selectedArr={selectedArr}
            selectedFunc={selectedHandler}
          />
        )}
        <ExercisesList
          bodyAreasArr={selectedArr}
          isMale={isMale}
          showDiary={showDiary}
        />
      </StyledContainer>
    </Container>
  );
};

export default Exercises;
