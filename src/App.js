import Theme from "./containers/theme";
import Exercises from "./containers/exercises";
import { Header } from "../src/components";

const App = () => {
  return (
    <Theme>
      <Header />
      <Exercises />
    </Theme>
  );
};

export default App;
