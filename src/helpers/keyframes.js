import { keyframes } from "styled-components";

export const slideLeft = keyframes`
0% {
  left: 45%;
}
33.33% {
  left: 30%;
}
66.66% {
  left: 15%;
}
100% {
  left: 0%;
}`;

export const slideRight = keyframes`
0% {
    left: 0%;
}
33.33% {
  left: 15%;
}
66.66% {
  left: 30%;
}
100% {
    left: 45%;
}`;
