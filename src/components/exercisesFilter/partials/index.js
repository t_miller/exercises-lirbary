import styled from "styled-components";

export const StyledContainer = styled.div`

  @media (min-width: ${props => props.theme.breakPoints.wide}) {
    width: 10%;
    position: fixed;
    left: 1rem;
    top: 30%;
  }

  @media (min-width: calc(${props => props.theme.breakPoints.wide} + 300px)) {
    top: 5rem;
  }

  @media (max-width: ${props => props.theme.breakPoints.wide}) {
    width: 100%;
  }
`;

export const StyledTagContainer = styled.div`
  @media (max-width: ${props => props.theme.breakPoints.wide}) {
    width: 100%;
    display: flex;
    flex-wrap: wrap;
    align-items: center;
  }
`;

export const StyledTag = styled.div`
  margin: 0.5rem 0;
  width: fit-content;

  @media (max-width: ${props => props.theme.breakPoints.wide}) {
    margin: 0.4rem 0.5rem 0.4rem 0;
  }

  & div {
    margin: 0;
    cursor: pointer;
    ${props =>
      props.isSelected ? `background: ${props.theme.colors.gray};` : ""}
  }
  & span {
    ${props => (props.isSelected ? `color: ${props.theme.colors.white};` : "")}
  }
`;

export const StyledTitle = styled.div`
  ${props => `${props.theme.typography.bodyLarge};
  color: ${props.theme.colors.gray};`}
  @media (max-width: ${props => props.theme.breakPoints.wide}) {
    align-items: center;
    text-align: center;
  }
`;
