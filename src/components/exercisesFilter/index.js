import React from "react";
import { exerciseTypes } from "../../helpers/exerciseTypes";
import { Tag } from "../";

import {
  StyledContainer,
  StyledTagContainer,
  StyledTag,
  StyledTitle
} from "./partials";

const ExercisesFilter = ({ selectedArr, selectedFunc }) => {
  return (
    <StyledContainer>
      <StyledTitle>Select body part</StyledTitle>
      <StyledTagContainer>
        {exerciseTypes.length > 0 &&
          exerciseTypes.map((e, i) => (
            <StyledTag
              isSelected={selectedArr.includes(e)}
              onClick={() => selectedFunc(e)}
              key={i}
            >
              <Tag label={e} />
            </StyledTag>
          ))}
      </StyledTagContainer>
    </StyledContainer>
  );
};

export default ExercisesFilter;
