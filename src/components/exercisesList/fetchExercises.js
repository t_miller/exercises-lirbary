import fetch from "cross-fetch";

export const fetchExercises = () => {
  return fetch(
    "https://private-922d75-recruitmenttechnicaltest.apiary-mock.com/customexercises/"
  )
    .then(res => res.json())
    .then(res => {
      return res.exercises;
    })
    .catch(error => console.error("Fetch error ", error));
};
