import styled from "styled-components";

export const StyledContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  width: 100%;

  @media (max-width: calc(${props => props.theme.breakPoints.wide} + 300px)) {
    width: 80%;
    margin 0 auto;
  }

  @media (max-width: calc(${props => props.theme.breakPoints.wide} + 64px)) {
    width: 100%;
  }
`;

export const StyledMessage = styled.div`
  margin-top: 2rem;
  display: flex;
  justify-content: center;
  width: 100%;
  ${props => props.theme.typography.body}
`;
