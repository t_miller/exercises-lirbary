import React, { useEffect, useState } from "react";
import { fetchExercises } from "./fetchExercises";
import ExercisesItem from "./exercisesItem";

import { StyledContainer, StyledMessage } from "./partials";

const ExercisesList = ({
  bodyAreasArr = [],
  isMale = true,
  showDiary = false
}) => {
  const [exercisesArr, SetExercisesArr] = useState([]);
  const [areasExercises, SetAreasExercises] = useState([]);
  const [userExercises, SetUserExercises] = useState([]);

  useEffect(() => {
    CallFetchExercises();
  }, []);

  useEffect(() => {
    if (bodyAreasArr.length > 0 && exercisesArr) {
      let arr = [];
      exercisesArr.forEach(
        e => e.bodyAreas.some(r => bodyAreasArr.includes(r)) && arr.push(e)
      );
      SetAreasExercises(arr);
    }
  }, [bodyAreasArr, exercisesArr]);

  const CallFetchExercises = async () => {
    const exercises = await fetchExercises();
    SetExercisesArr(exercises);
  };

  const userExercisesHandler = ex => {
    const newArr = userExercises.includes(ex)
      ? userExercises.filter(e => e !== ex)
      : [...userExercises, ex];
    SetUserExercises(newArr);
  };

  const areasArrIsEmpty = bodyAreasArr && bodyAreasArr.length === 0;

  return (
    <StyledContainer>
      {showDiary ? (
        userExercises.length > 0 ? (
          userExercises.map(e => (
            <ExercisesItem
              key={e.id}
              {...e}
              isMale={isMale}
              userRemove={() => userExercisesHandler(e)}
              showRemove={true}
            />
          ))
        ) : (
          <StyledMessage>
            <span>Please go back and select exercises you wish to add</span>
          </StyledMessage>
        )
      ) : (
        <>
          <>
            {areasArrIsEmpty &&
              exercisesArr.length > 0 &&
              exercisesArr.map(e => (
                <ExercisesItem
                  key={e.id}
                  {...e}
                  isMale={isMale}
                  userSelect={() => userExercisesHandler(e)}
                  clickable={true}
                  isSelected={userExercises.includes(e)}
                />
              ))}
          </>
          <>
            {areasExercises.length > 0 &&
              areasExercises.map(e => (
                <ExercisesItem
                  key={e.id}
                  {...e}
                  isMale={isMale}
                  userSelect={() => userExercisesHandler(e)}
                  clickable={true}
                  isSelected={userExercises.includes(e)}
                />
              ))}
          </>
        </>
      )}
    </StyledContainer>
  );
};

export default ExercisesList;
