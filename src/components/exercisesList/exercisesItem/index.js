import React from "react";
import parse from "html-react-parser";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimesCircle } from "@fortawesome/free-regular-svg-icons";
import PropTypes from "prop-types";
import { Tag } from "../../";

import {
  StyledExercisesItem,
  StyledTitle,
  StyledImage,
  StyledTranscript,
  StyledTagContainer,
  StyledInner,
  StyledRemove,
  StyledRemoveInner
} from "./partials";

const ExercisesItem = ({
  bodyAreas = [],
  female,
  male,
  name,
  transcript,
  isMale,
  userSelect,
  userRemove,
  clickable = false,
  isSelected,
  showRemove = false
}) => {
  const imageSrc = isMale ? male && male.image : female && female.image;

  const selectHandler = () => {
    if (userSelect) {
      userSelect();
    }
  };

  return (
    <StyledExercisesItem
      isClickable={clickable}
      onClick={selectHandler}
      isSelected={isSelected}
    >
      {imageSrc && (
        <StyledImage>
          <img src={imageSrc} alt={name} />
        </StyledImage>
      )}
      <StyledInner>
        <StyledTitle>{name}</StyledTitle>
        <StyledTagContainer>
          {bodyAreas.length > 0 &&
            bodyAreas.map((e, i) => <Tag key={i} label={e} />)}
        </StyledTagContainer>
        <StyledTranscript>{parse(transcript)}</StyledTranscript>
        {showRemove && (
          <StyledRemove>
            <StyledRemoveInner onClick={userRemove}>
              <FontAwesomeIcon icon={faTimesCircle} />
              <span>remove</span>
            </StyledRemoveInner>
          </StyledRemove>
        )}
      </StyledInner>
    </StyledExercisesItem>
  );
};

ExercisesItem.propTypes = {
  bodyAreas: PropTypes.arrayOf(String),
  name: PropTypes.string,
  male: PropTypes.object,
  female: PropTypes.object,
  transcript: PropTypes.string
};

export default ExercisesItem;
