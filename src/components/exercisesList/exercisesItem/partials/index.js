import styled from "styled-components";

export const StyledExercisesItem = styled.div`
  width: calc(33.33% - 1.333rem);
  margin: 1rem;
  background: ${props => props.theme.colors.white};
  box-shadow: 0px 3px 6px #20212129;
  ${props =>
    props.isSelected ? `border: 2px solid ${props.theme.colors.primary};` : ""}

  ${props =>
    props.isClickable
      ? `cursor: pointer;`
      : ""} 

  &:nth-of-type(3n + 4), :first-of-type {
    margin: 1rem 1rem 1rem 0;
  }

  &:nth-of-type(3n) {
    margin: 1rem 0 1rem 1rem;
  }

  @media (max-width: ${props => props.theme.breakPoints.tablet}) {
    width: calc(50% - 1.333rem);

    &:nth-of-type(2n + 3),
    :first-of-type {
      margin: 1rem 1rem 1rem 0;
    }

    &:nth-of-type(2n) {
      margin: 1rem 0 1rem 1rem;
    }
  }
  @media (max-width: ${props => props.theme.breakPoints.mobile}) {
    width: 100%;

    &:nth-of-type(1n),
    :first-of-type {
      margin: 1rem 0;
    }
  }
`;

export const StyledInner = styled.div`
  padding: 0.5rem;
`;

export const StyledTitle = styled.div`
  ${props => props.theme.typography.bodyLarge}
`;

export const StyledImage = styled.div`
  & img {
    width: 100%;
  }
`;

export const StyledTranscript = styled.div`
  ${props => props.theme.typography.body}
`;

export const StyledTagContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin: 1rem 0;
`;

export const StyledRemove = styled.div`
  display: flex;
  justify-content: center;
`;

export const StyledRemoveInner = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0.5rem 1rem;
  border: 2px solid ${props => props.theme.colors.red};
  border-radius: 45px;
  cursor: pointer;

  & svg {
    color: ${props => props.theme.colors.red};
    margin-right: 0.5rem;
  }
  & span {
    color: ${props => props.theme.colors.red};
  }
`;
