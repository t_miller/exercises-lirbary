import React from "react";
import { StyledContainer } from "./partials";

const Container = ({ children }) => {
  return <StyledContainer>{children}</StyledContainer>;
};

export default Container;
