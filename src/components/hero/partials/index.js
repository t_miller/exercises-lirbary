import styled from "styled-components";

export const StyledHero = styled.div`
  ${props => props.theme.typography.heading};
`;
