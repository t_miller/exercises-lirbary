import React, { useState } from "react";
import {
  StyledContainer,
  StyledInner,
  StyledLabel,
  StyledSlider
} from "./partials";

const Switch = ({ switchHandler }) => {
  const [slide, SetSlide] = useState("");

  const onClickHandler = () => {
    SetSlide(slide === "left" ? "right" : "left");
    switchHandler();
  };

  return (
    <StyledContainer onClick={onClickHandler}>
      <StyledSlider slide={slide} />
      <StyledInner gender="male">
        <StyledLabel>
          <span>Male</span>
        </StyledLabel>
      </StyledInner>
      <StyledInner>
        <StyledLabel>
          <span>Female</span>
        </StyledLabel>
      </StyledInner>
    </StyledContainer>
  );
};

export default Switch;
