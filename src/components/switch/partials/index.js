import styled, { css } from "styled-components";
import { slideLeft, slideRight } from "../../../helpers/keyframes";

export const StyledContainer = styled.div`
  position: relative;
  display: flex;
  justify-content: space-between;
  border-radius: 15px;
  border: none;
  text-align: center;
  box-shadow: 0px 3px 6px #20212129;
  cursor: pointer;
  width: 20%;

  @media (max-width: ${props => props.theme.breakPoints.tablet}) {
    width: 33.33%;
  }

  @media (max-width: ${props => props.theme.breakPoints.mobile}) {
    width: 50%;
  }
`;

export const StyledInner = styled.div`
  width: 50%;
  display: flex;
  padding: 0.5rem;
  border: none;

  ${props =>
    props.gender === "male"
      ? `
  border-radius: 15px 0 0 15px;
  justify-content: flex-start;
  background: rgba(137, 196, 244, 1);`
      : `
  border-radius: 0 15px 15px 0;
  justify-content: flex-end;
  background: pink;`}
`;

export const StyledLabel = styled.div`
  ${props =>
    `${props.theme.typography.body}; color: ${props.theme.colors.white}`}
`;

export const StyledSlider = styled.div`
  position: absolute;
  left: 45%;
  width: 55%;
  height: 100%;
  border-radius: 15px;
  border: none;
  background: ${props => props.theme.colors.white};

  ${props =>
    props.slide === "right" &&
    css`
      animation: ${slideRight} 0.25s linear 0s forwards;
    `}

  ${props =>
    props.slide === "left" &&
    css`
      animation: ${slideLeft} 0.25s linear 0s forwards;
    `}
`;
